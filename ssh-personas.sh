echo "======================================================================================================="
echo "WARNING : This program will rewrite ssh-keys and keys associated with ssh-agent! Press CTRL + C to exit"
echo "======================================================================================================="
echo "Enter id for personal Account: "
read personal
echo "Enter id for Pathashala ID: "
read work
if [ ! -d ~/.ssh ] ; then
	mkdir ~/.ssh
fi
cmd="ssh-keygen -f ~/.ssh/$personal -C '$personal' -N ''"
bash -c "$cmd"
cmd="ssh-keygen -f ~/.ssh/$work -C '$work' -N ''"
bash -c "$cmd"
if [ ! -f ~/.ssh/config ] ; then
	touch ~/.ssh/config
fi
echo "HostName defaults to bitbucket.org"
personalFilePath="~/.ssh/$personal"
printf "Host %s\n\tHostName bitbucket.org\n\tIdentityFile %s\n" "$personal" "$personalFilePath" > ~/.ssh/config
workFilePath="~/.ssh/$work"
printf "Host %s\n\tHostName bitbucket.org\n\tIdentityFile %s\n" "$work" "$workFilePath" >> ~/.ssh/config
echo "Added hosts to config file"
cmd="ssh-add -D"
bash -c "$cmd"
cmd="ssh-add ~/.ssh/$work"
bash -c "$cmd"
cmd="ssh-add ~/.ssh/$personal"
bash -c "$cmd"
echo "Added keys to ssh-agent"
echo "Your personal public key is: "
cmd="cat ~/.ssh/$personal.pub"
bash -c "$cmd"
echo "Your work public key is: "
cmd="cat ~/.ssh/$work.pub"
bash -c "$cmd"